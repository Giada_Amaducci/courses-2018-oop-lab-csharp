﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Indexer
{

    public class Map2D<TKey1, TKey2, TValue> : IMap2D<TKey1, TKey2, TValue>
    {
        private IDictionary<Tuple<TKey1,TKey2>, TValue> map = new Dictionary<Tuple<TKey1, TKey2>, TValue>();

        public bool Equals(IMap2D<TKey1, TKey2, TValue> other)
        {
            if (other == null || !(other is Map2D<TKey1, TKey2, TValue>))
                return false;
            else
                return true;
        }

        public TValue this[TKey1 key1, TKey2 key2]
        {
            get
            {
                /* IList<Tuple<TKey2, TValue>> row = GetRow(key1);
                 for (int i=0; i<row.Count(); i++)
                 {
                     if (row[i].Item1.Equals(key2))
                     {
                         return row[i].Item2;
                     }
                 }
                 return default(TValue);*/
                Tuple<TKey1, TKey2> key = new Tuple<TKey1, TKey2>(key1, key2);
                return map[key];
            }
            set
            {
                Tuple<TKey1, TKey2> key = new Tuple<TKey1, TKey2>(key1, key2);
                map[key] = value;
            }
        }

        public IList<Tuple<TKey2, TValue>> GetRow(TKey1 key1)
        {
            IList<Tuple<TKey2, TValue>> row = new List<Tuple<TKey2, TValue>>();
            foreach (var elem in map)
            {
                if (elem.Key.Item1.Equals(key1))
                {
                    row.Add(new Tuple<TKey2, TValue>(elem.Key.Item2, elem.Value));
                }
            }
            return row;
        }

        public IList<Tuple<TKey1, TValue>> GetColumn(TKey2 key2)
        {
            IList<Tuple<TKey1, TValue>> column = new List<Tuple<TKey1, TValue>>();
            foreach (var elem in map)
            {
                if (elem.Key.Item2.Equals(key2))
                {
                    column.Add(new Tuple<TKey1, TValue>(elem.Key.Item1, elem.Value));
                }
            }
            return column;
        }

        public IList<Tuple<TKey1, TKey2, TValue>> GetElements()
        {
            IList<Tuple<TKey1, TKey2, TValue>> list = new List<Tuple<TKey1, TKey2, TValue>>();
            foreach (var elem in map)
            {
                list.Add(new Tuple<TKey1, TKey2, TValue>(elem.Key.Item1, elem.Key.Item2, elem.Value));
            }
            return list;

        }

        public void Fill(IEnumerable<TKey1> keys1, IEnumerable<TKey2> keys2, Func<TKey1, TKey2, TValue> generator)
        {
            foreach (TKey1 key1 in keys1)
            {
                foreach (TKey2 key2 in keys2)
                {

                    map = new Tuple<TKey1, TKey2>(key1, key2), generator(key1, key2);
                }
            }
        }

        public int NumberOfElements
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
